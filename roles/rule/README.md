# Ansible Role: scc\_net.firewall.rule

Create ferm input rules and restart ferm

## Dependencies

 - scc_net.firewall.ferm

## Role Variables

```
rule_name:
rule_priority: 10
rule_input_rules:
  - description:
    protocol:
    from:
    port:
    action: ACCEPT
```

## Example

```
rule_name: Example Rule Name
rule_priority: 10
rule_input_rules:
  - description: Example Rule Description Part 1
    protocol: tcp
    from:
      - 1.1.1.1
    port:
      - 80
      - 443
  - description: Example Rule Description Part 2
    protocol: udp
    from:
      - 1.2.3.4
    port:
      - 9000
```

# Ansible Role: scc\_net.firewall.ferm

Installs and configures ferm

## Dependencies

 - none

## Role Variables

```
ferm_enabled: True

ferm_sshd_rule: True
#ferm_sshd_interface:
#ferm_sshd_allowed_from:
```
